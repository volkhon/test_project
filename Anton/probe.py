# from dis import dis
#
# def some_func(param):
#     a = 99
#     print(a, param, a-1)
#     return a
# dis(some_func)

import simple_draw as sd


def liner():
    point1 = sd.random_point()
    point2 = sd.random_point()
    x = point1.x
    y = point2.y
    point3 = sd.get_point(x=x, y=y)
    point_list = [point1, point2, point3]
    color = sd.random_color()

    sd.lines(point_list=point_list, color=color,
             closed=False, width=sd.random_number(1, 4))

# комментарий здесь
