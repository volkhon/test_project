# -*- coding: utf-8 -*-

class NotNameError(Exception):
    pass


class NotEmailError(Exception):
    pass


def check(line):
    if len(line.split(' ')) != 3:
        raise ValueError('НЕ присутствуют все три поля:                ValueError')
    print(f'Read line {line}', flush=True)
    v_1, v_2, v_3 = line.split(' ')
    v_1 = str(v_1)
    v_2 = str(v_2)
    v_3 = str(v_3)
    if not v_1.isalpha():
        raise NotNameError('поле имени содержит НЕ только буквы:         NotNameError')
    if not ("@" in v_2 or '.' in v_2):
        raise NotEmailError("поле email НЕ содержит @ и .(точку):         NotEmailError")
    try:
        v_3 = int(v_3)
    except ValueError:
        raise ValueError('поле возраст НЕ является числом:             ValueError')
    else:
        if v_3 < 10 or 99 < v_3:
            raise ValueError('поле возраст НЕ является числом от 10 до 99: ValueError')
    res = line
    return res


f1 = open('registrations_good.log', 'w', encoding='utf8')
f2 = open('registrations_bad.log', 'w', encoding='utf8')
with open('registrations.txt', 'r', encoding='utf8') as ff:
    for line in ff:
        l = len(line)  # это для красоты вывода, можно убрать
        line = line[:-1]
        try:
            f1.write(check(line) + '\n')
        except ValueError as exc:
            f2.write(line + ' ' * (40 - l) + f'Ошибка: {exc}' + '\n')  # '* (40 - l) - для красоты вывода
        except NotNameError as exc:
            f2.write(line + ' ' * (40 - l) + f'Ошибка: {exc}' + '\n')
        except NotEmailError as exc:
            f2.write(line + ' ' * (40 - l) + f'Ошибка: {exc}' + '\n')
f1.close()
f2.close()
